#!/bin/bash
#Installs test components for build
#Laravel must already be set up

#install base laravel
cd /var/www
laravel new vanilla_taste
chmod -R 775 vanilla_taste
chown -R www-data:www-data vanilla_taste 

#create a soft link
cd /var/www/html
ln -s /var/www/vanilla_taste/public test


