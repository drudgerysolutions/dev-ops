#!/bin/bash
# Install NODEJS and NVM
# Docs https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/

sudo apt-get install python-software-properties
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install nodejs

