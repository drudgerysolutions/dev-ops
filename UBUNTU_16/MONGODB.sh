#!/bin/bash
# Install Mongodb in Ubuntu 16 during the install process
#https://medium.com/@rafaelcpalmeida/how-to-use-mongodb-with-your-lumen-api-e13f36fa0aa6
#https://github.com/jenssegers/laravel-mongodb

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update

sudo apt-get install -y mongodb-org

cd /lib/systemd/system/

# nano mongod.service
# [Unit]
# Description=High-performance, schema-free document-oriented database
# After=network.target
# Documentation=https://docs.mongodb.org/manual

# [Service]
# User=mongodb
# Group=mongodb
# ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

# [Install]
# WantedBy=multi-user.target

systemctl daemon-reload

systemctl start mongod
systemctl enable mongod

netstat -plntu

mongo
use admin

db.createUser({user:"workflowuser", pwd:"qwerty123123", roles:[{role:"root", db:"admin"}]})

exit

nano /lib/systemd/system/mongod.service

echo "You need to change the line ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf \nto\n ExecStart=/usr/bin/mongod --quiet --auth --config /etc/mongod.conf"

systemctl daemon-reload

service mongod restart

mongo -u root -p qwerty123!@# --authenticationDatabase admin


sudo apt-get install php-mongodb

composer require jenssegers/mongodb