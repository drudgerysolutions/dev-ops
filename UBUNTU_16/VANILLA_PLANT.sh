#!/bin/bash
#Installs the development components
#Laravel must already be set up

#install base laravel
cd /var/www
laravel new vanilla_plant
chmod -R 775 vanilla_plant
chown -R www-data:www-data vanilla_plant 

#create a soft link
cd /var/www/html
ln -s /var/www/vanilla_plant/public dev

cd ~/dev-ops/UBUNTU_16
source PHPMYADMIN.sh


cat ~/dev-ops/database_scripts/development.sql | mysql -u root -p 