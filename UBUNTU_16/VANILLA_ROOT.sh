#!/bin/bash
#Vanilla root is the application which will go into production
#Laravel must already be set up

#install base laravel
cd /var/www
laravel new vanilla_root
chmod -R 775 vanilla_root
chown -R www-data:www-data vanilla_root 

#create a soft link from the default web root and call it app
cd /var/www/html
ln -s /var/www/vanilla_root/public app

cat ~/dev-ops/database_scripts/system.sql | mysql -u root -p 