#!/bin/bash
# Install composer in Ubuntu 16

#your standard thing to do to get the latest stable
apt-get update

#composer won't work properly without these
apt-get install php-zip unzip

cd ~

curl -sS https://getcomposer.org/installer -o composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
